Project
=======

- Project Confluence Page => https://mgonline.atlassian.net/wiki/spaces/EA/pages/680099944/India+Data+Storage+Project

- Infrastructure as Code: Terraform is being used for provisioning AWS resources (e.g S3 buckets ...)

How to run it?
==============

1. Set AWS environment variables. For example,
   
   export AWS_ACCESS_KEY_ID = <YOUR-AWS-ACCOUNT-ACCESS-ID?
   
   export AWS_SECRET_ACCESS_KEY = <YOUR-AWS-ACCOUNT-SECRET-ACCESS-KEY>

2. Install Terraform => https://www.terraform.io/downloads.html 

3. Terraforms state has been persisted in S3 bucket, please have your admin to add your user account to MGITerraformDevGroup

4. git clone https://GJanarthanam@bitbucket.org/GJanarthanam/mgi-india-datastorage-project.git

5. cd mgi-india-datastorage-project

6. Enter:  

    terraform init  -> Initialize the environment
	terraform plan  -> lists execution plan
	terraform apply -> Executes the plan


Need help?
==========

Please contact Jana (GJanarthanam@moneygram.com) if you need help or have any questions.