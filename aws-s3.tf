terraform {
  required_version = ">= 0.11.7, < 0.11.8"

  // Terraform state is stored in S3 bucket to support group development.
  backend "s3" {
    bucket = "mgi-india-data-storage-terraform-state"
    key = "mgi-india-tf-state"
    region = "us-east-2"
    encrypt = true
  }
}

provider "aws" {
  region = "${var.region}"
  version = "~> 1.29.0"
}

// 1. S3 bucket - india storage files
resource "aws_s3_bucket" "india-data-storage-project" {
  bucket = "${var.mgi-india-data-storage-bucket-name}"
  acl = "private"

  versioning {
    enabled = true
  }

  tags {
    Name = "mgi"
  }

  logging {
    target_bucket = "${aws_s3_bucket.accesslogs_bucket.id}"
    target_prefix = "mgi-india/"
  }

  server_side_encryption_configuration {
    "rule" {
      "apply_server_side_encryption_by_default" {
        sse_algorithm = "AES256"
      }
    }
  }
}

// 2. S3 bucket - access logs
resource "aws_s3_bucket" "accesslogs_bucket" {
  bucket = "${var.accesslogs-bucket-name}"
  acl = "log-delivery-write"
}

// Policy Settings: RBI Regulators Group Policy (Read Only)
resource "aws_iam_group" "regulators" {
  name = "RBIUsersGroup"
}
resource "aws_iam_group_policy" "read-policy" {
  name = "RBIUsersGroupReadOnlyPolicy"
  group = "${aws_iam_group.regulators.name}"
  policy = "${file("read_policy.json")}"
}

// Policy Settings: MGI Group Policy (Write)
resource "aws_iam_group" "mgi-data-transmission-group" {
  name = "MGIDataTransmissionGroup"
}
resource "aws_iam_group_policy" "write-policy" {
  name = "MGIDataAccessGroupWritePolicy"
  group = "${aws_iam_group.mgi-data-transmission-group.name}"
  policy = "${file("write_policy.json")}"
}

// Policy Settings: Terraform state S3 bucket policy
resource "aws_iam_group" "terraform-dev-group" {
  name = "MGITerraformDevGroup"
}
resource "aws_iam_group_policy" "dev-policy" {
  name = "MGITerraformDevGroupPolicy"
  group = "${aws_iam_group.terraform-dev-group.name}"
  policy = "${file("tf_dev_policy.json")}"
}


//  S3 Each notification is delivered as a JSON object with the following fields:
//  Region
//  Timestamp
//  Event Type (as listed above)
//  Request Actor Principal ID
//  Source IP of the request
//  Request ID
//  Host ID
//  Notification Configuration Destination ID
//  Bucket Name
//  Bucket ARN
//  Bucket Owner Principal ID
//  Object Key
//  Object Size
//  Object ETag
//  Object Version ID (if versioning is enabled on the bucket)

// Note: Terraform doesn't support SNS Subscription(Email protocol) - hence, it was configured manually.

resource "aws_sns_topic" "topic" {
  name = "s3-event-notification-topic"

  policy = <<POLICY
  {
    "Version":"2012-10-17",
    "Statement":[{
        "Effect": "Allow",
        "Principal": {"AWS":"*"},
        "Action": "SNS:Publish",
        "Resource": "arn:aws:sns:*:*:s3-event-notification-topic",
        "Condition":{
            "ArnLike":{"aws:SourceArn":"${aws_s3_bucket.india-data-storage-project.arn}"}
        }
    }]
  }
    POLICY
  }


resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "${aws_s3_bucket.india-data-storage-project.id}"

  topic {
    topic_arn = "${aws_sns_topic.topic.arn}"
    events = [
      "s3:ObjectCreated:*",
      "s3:ObjectRemoved:*"]
    //    filter_suffix = ".log"
  }
}

