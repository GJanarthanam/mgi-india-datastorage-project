variable "mgi-india-data-storage-bucket-name" {
  default = "mgi-data-rbi-india-storage-bucket"
  description = "Persist data (Mumbai region) to be compliant with RBI requirements"
}

variable "accesslogs-bucket-name" {
  default = "mgi-data-rbi-india-storage-bucket-access-logs"
  description = "access logs for auditing purpose"
}

variable "region" {
  default = "ap-south-1"
  description = "India Mumbai Region"
}
